﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mongo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Init();
        }

        private static string connectionString = "mongodb://localhost:27017";
        private static IMongoDatabase db;


        private void button1_Click(object sender, EventArgs e)
        {
            var limit = textBox2.Text;
            if (limit.Length == 0)
            {
                GetAll();
            }
            else
            {
                GetAll(int.Parse(limit));
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var Name = tx_name.Text;
            var Surname = tx_surname.Text;

            var col = db.GetCollection<User>("User");

            List<User> List = new List<User>();
            if (Name.Length > 0 && Surname.Length > 0)
            {
                List = col.AsQueryable<User>().Where<User>(sb => sb.name == Name).Where<User>(sb => sb.Surname == Surname).ToList();
            }
            else
            if (Name.Length > 0)
            {
                List = col.AsQueryable<User>().Where<User>(sb => sb.name == Name).ToList();
            }
            else
            if (Surname.Length > 0)
            {
                List = col.AsQueryable<User>().Where<User>(sb => sb.Surname == Surname).ToList();
            }

            textBox1.Text = "ID                       Name      Surname      Age \n";
            foreach (var item in List)
            {
                textBox1.AppendText(item.Id + "    \n" + item.name + "  " + item.Surname + "   " + item.Age + "\n");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var id = tx_id.Text;
            ObjectId objectId = ObjectId.Parse(id);

            var col = db.GetCollection<User>("User");
            try
            {
                col.DeleteOne(Item => Item.Id == objectId);
            }
            catch (Exception ex)
            {
                textBox1.Text = "Cant Delete by this id";
            }
            finally
            {
                GetAll();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var Name = tx_add_name.Text;
            var Surname = tx_add_surname.Text;
            var Age = int.Parse(tx_add_age.Text);

            var entity = new User { name = Name, Surname = Surname, Age = Age };

            var col = db.GetCollection<User>("User");
            col.InsertOne(entity);

            GetAll();
        }

        private void Init()
        {
            MongoClient client = new MongoClient(connectionString);
            db = client.GetDatabase("LAB");
        }

        private void GetAll()
        {
            var col = db.GetCollection<User>("User");
            var List = col.Find(_ => true).ToList();
            textBox1.Text = "ID                       Name      Surname      Age \n";
            foreach (var item in List)
            {
                textBox1.AppendText(item.Id + " \n " + item.name + "  " + item.Surname + "   " + item.Age + "\n");
            }
        }

        private void GetAll(int limit)
        {
            var col = db.GetCollection<User>("User");
            var List = col.Find(_ => true).Limit(limit).ToList();
            textBox1.Text = "ID                       Name      Surname      Age \n";
            foreach (var item in List)
            {
                textBox1.AppendText(item.Id + " \n " + item.name + "  " + item.Surname + "   " + item.Age + "\n");
            }
        }

    }
}
