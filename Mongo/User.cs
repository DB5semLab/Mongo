﻿using MongoDB.Bson;
using System.Collections.Generic;

namespace Mongo
{
    class User
    {
        public ObjectId Id { get; set; }
        public string name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
    }
}
